import React, { Suspense } from "react";
import * as RouterDom from "react-router-dom";
import { Router } from "../config";
import { BrowserRouter } from "react-router-dom";
import { DefaultHeader } from ".";

export default function DefaultLayout(props: any) {
  const loading = () => (
    <div className="animated fadeIn pt-1 text-center p-5">Loading...</div>
  );
  return (
    <BrowserRouter>
      <div className="app">
        <div className="app-header">
          <DefaultHeader />
        </div>
        <div className="app-body">
          <main className="main">
            <div className="app-overlay">
              <div className="app-content">
                <Suspense fallback={loading()}>
                  <RouterDom.Switch>
                    {Router.map((route, idx) =>
                      route.component ? (
                        <RouterDom.Route
                          key={idx} // eslint-disable-line
                          path={route.path}
                          exact={route.exact}
                          name={route.name}
                          render={(props: any) => (
                            <route.component {...props} />
                          )}
                        />
                      ) : null
                    )}
                    <RouterDom.Redirect from="/" to="/home" />
                  </RouterDom.Switch>
                </Suspense>
              </div>
            </div>
          </main>
        </div>
      </div>
    </BrowserRouter>
  );
}
