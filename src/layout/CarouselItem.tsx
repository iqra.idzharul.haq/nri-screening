import React from "react";
import { observer } from "mobx-react-lite";

const CarouselItem = observer((props: any) => {
  return (
    <div className={props.active ? "carousel-active" : "carousel-item"}>
      <img src={props.item.url} alt="cover"></img>
      {props.active ? <p>{props.item.title}</p> : ""}
    </div>
  );
});

export default CarouselItem;
