import React, { useEffect, useState, useCallback, useRef } from "react";
import { autorun } from "mobx";
import { observer } from "mobx-react-lite";
import { useStore } from "../services/store";
import CarouselItem from "./CarouselItem";

const CarouselAlbum = observer((props: any) => {
  const store = useStore();
  const [cover, setCover] = useState<any[]>([]);
  const carouselContainer = useRef(document.createElement("div"));

  const pickCover = useCallback(() => {
    let cover: any[] = [];
    for (let i = 0; i < 10; i++) {
      const randomElement =
        store.album[i][Math.floor(Math.random() * store.album[i].length)];
      cover = [...cover, randomElement];
    }
    setCover(cover);
  }, [store]);

  useEffect(
    () =>
      autorun(() => {
        if (store.album.length) {
          pickCover();
        }
      }),
    [pickCover, store]
  );

  return (
    <div ref={carouselContainer} className="carousel-container">
      {cover &&
        cover.map((item, index) => {
          if (index === store.selectedAlbum) {
            carouselContainer.current.style.left = `${50 - index * 190}px`;
            return <CarouselItem active={true} item={item} />;
          } else {
            if (index > store.selectedAlbum + 1) {
              return "";
            }
            return (
              <img
                className="image-carousel"
                src={item.url}
                alt="cover"
                key={item.id}
                onClick={() => {
                  store.setSelectedAlbum(index);
                }}
              ></img>
            );
          }
        })}
    </div>
  );
});

export default CarouselAlbum;
