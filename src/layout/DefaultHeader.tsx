import React, { useRef } from "react";
import { HeaderContent, CarouselAlbum } from ".";
import { observer } from "mobx-react-lite";
import { useStore } from "../services/store";

const DefaultHeader = observer((props: any) => {
  const store = useStore();
  const carousel = useRef(document.createElement("div"));
  const headerContent = useRef(document.createElement("div"));
  const albumIndicator = useRef(document.createElement("div"));

  let prevScrollpos = window.pageYOffset;

  window.onscroll = () => {
    let currentScrollPos = window.pageYOffset;
    if (prevScrollpos > currentScrollPos) {
      headerContent.current.style.top = "0px";
      carousel.current.style.top = "120px";
      albumIndicator.current.style.top = "240px";
    } else {
      headerContent.current.style.top = "-120px";
      carousel.current.style.top = "0px";
      albumIndicator.current.style.top = "120px";
    }
    prevScrollpos = currentScrollPos;
  };

  return (
    <div className="header">
      <div ref={headerContent} className="header-content">
        <HeaderContent />
      </div>
      <div ref={carousel} className="carousel">
        <CarouselAlbum />
        <div ref={albumIndicator} className="page-indicator">
          {" "}
          {store.selectedAlbum + 1} of 10
        </div>
      </div>
    </div>
  );
});

export default DefaultHeader;
