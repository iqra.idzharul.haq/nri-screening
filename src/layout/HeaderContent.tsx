import React from "react";
import ProfilePic from "../assets/img/profile.jpg";
import email from "../assets/img/email.svg";
import location from "../assets/img/Shape.svg";
import verified from "../assets/img/verified.svg";
import menu from "../assets/img/Menu.svg";

export default function HeaderContent(props: any) {
  return (
    <div>
      <img className="profile-picture" src={ProfilePic} alt="profile" />
      <div>
        <div className="profile-name">
          Melanie Tan &nbsp;
          <img src={verified} alt="location" />
        </div>
        <div className="profile-description">
          Professional Food Photographer
        </div>
        <div className="profile-details">
          <div>
            <img src={location} alt="location" />
            <p>&ensp;Bangkok</p>
          </div>
          <div>
            <img src={email} alt="email" />
            <p>&ensp;melanietan99@gmail.com</p>
          </div>
        </div>
      </div>
      <div className="menu">
        <img src={menu} alt="menu" />
      </div>
    </div>
  );
}
