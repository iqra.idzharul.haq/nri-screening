import React, { useEffect } from "react";
import { observer } from "mobx-react-lite";
import { useStore } from "../../../services/store";
import InfiniteScrollComponent from "../components/InfiniteScrollComponent";

const Home = observer(() => {
  const store = useStore();
  useEffect(() => {
    store.fetchPotos();
  }, [store]);

  return (
    <div className="animated fadeIn">
      <div className="content">
        <InfiniteScrollComponent even={false} />
        <InfiniteScrollComponent even={true} />
      </div>
    </div>
  );
});

export default Home;
