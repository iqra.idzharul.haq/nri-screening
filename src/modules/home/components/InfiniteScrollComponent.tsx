import React, { useEffect, useState } from "react";
import { observer } from "mobx-react-lite";
import { useStore } from "../../../services/store";
import InfiniteScroll from "react-infinite-scroll-component";
import { autorun } from "mobx";

const InfiniteScrollComponent = observer((props: any) => {
  const store = useStore();
  const [items, setItems] = useState<any[]>([]);
  const [loaded, setIsLoaded] = useState(false);

  const fetchItems = () => {
    setTimeout(() => {
      setItems([
        ...items,
        ...store.album[store.selectedAlbum].slice(
          items.length % store.album[store.selectedAlbum].length,
          (items.length % store.album[store.selectedAlbum].length) + 20
        ),
      ]);
      setIsLoaded(true);
    }, 100);
  };

  useEffect(
    () =>
      autorun(() => {
        if (store.album.length) {
          setItems([
            ...items,
            ...store.album[store.selectedAlbum].slice(
              items.length,
              items.length + 20
            ),
          ]);
          setIsLoaded(true);
        }
      }),
    [store, setItems, items]
  );

  return (
    <div
      className={props.even ? "animated fadeIn scroll-even" : "animated fadeIn"}
    >
      <InfiniteScroll
        dataLength={items.length}
        next={() => fetchItems()}
        hasMore={true}
        loader={<p></p>}
      >
        <div className="image-grid" style={{ marginTop: "30px" }}>
          {loaded
            ? items.map((item: any) => {
                if (props.even) {
                  if (item.id % 2 !== 1) {
                    return (
                      <img
                        className="image-content"
                        src={item.url}
                        alt="even"
                        key={item.id}
                      ></img>
                    );
                  } else {
                    return "";
                  }
                } else {
                  if (item.id % 2 === 1) {
                    return (
                      <img
                        className="image-content"
                        src={item.url}
                        alt="odd"
                        key={item.id}
                      ></img>
                    );
                  } else {
                    return "";
                  }
                }
              })
            : ""}
        </div>
      </InfiniteScroll>
    </div>
  );
});

export default InfiniteScrollComponent;
