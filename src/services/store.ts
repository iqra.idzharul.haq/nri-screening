import { createContext, useContext } from "react";
import { action, observable } from "mobx";
import Axios from "axios";

export class Store {
  @observable album: any[] = [];
  @observable isLoading = true;
  @observable error = "";
  @observable selectedAlbum = 0;

  @action
  async fetchPotos() {
    try {
      this.album = [];
      this.isLoading = true;
      const resData = await Axios.get(
        "https://jsonplaceholder.typicode.com/photos"
      );
      if (resData.status === 200) {
        let album: any[] = [];
        for (let i = 1; i <= 10; i++) {
          const photos = resData.data.filter((item: any) => {
            return item.albumId === i;
          });

          album = [...album, photos];
        }
        this.album = album;
        this.isLoading = false;
        this.error = "";
      } else {
        this.error = `Error when fetching photos data with error status: ${resData.status}`;
        this.isLoading = false;
      }
    } catch (e) {
      this.error = `Error when fetching photos data with error: ${e}`;
      this.isLoading = false;
    }
  }

  @action
  setSelectedAlbum(index: number) {
    this.selectedAlbum = index;
  }
}
const StoreContext = createContext<Store>({} as Store);

export const StoreProvider = StoreContext.Provider;

export const useStore = (): Store => useContext(StoreContext);
